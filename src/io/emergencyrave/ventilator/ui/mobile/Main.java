package io.emergencyrave.ventilator.ui.mobile;


import com.codename1.ui.Display;
import com.codename1.ui.Form;
import userclasses.StateMachine;
import com.codename1.io.Log;
import com.codename1.ui.Toolbar;

/**
 * Main class
 * 
 * @author hhaus
 */
public class Main {
   
    private Form current;

    public void init(Object context) {
        // Enable Toolbar on all Forms by default
        Toolbar.setGlobalToolbar(true);

    }

    public void start() {
        if(current != null){
            current.show();
            return;
        }
        new StateMachine("/theme");        
    }

    public void stop() {
        current = Display.getInstance().getCurrent();
    }
    
    public void destroy() {
    }
}
